#ifndef PLANSZA_H
#define PLANSZA_H

#include <string>
#include <fstream>
#include <SFML/Graphics.hpp>
class Plansza
{
public:
    Plansza();

    void WczytajPlansze(std::string NazwaMapy);
    enum TypPola { NONE , GRASS , DIRT , WALL, NONE5 , NONE6 , NONE7, NONE8 ,WALLLT,WALLT,WALLRT,WALLL,WALLLB,WALLB,WALLRB,WARRR, COUNT};
    struct Pole
    {
        TypPola typ;
        bool Sciana;
    };
    void draw(sf::RenderWindow & win);
    sf::Sprite getSprite(int i , int j);
    const  static int width = 32;
    const static int height=24;
    const static int tile_width=32;
    const static int tile_height=32;
    Pole getPoziom (int x , int y);

protected:
private:
    Pole poziom[height][width];
    sf::Texture textura[COUNT];
    sf::Sprite sprite[24][32];

};

#endif // PLANSZA_H
