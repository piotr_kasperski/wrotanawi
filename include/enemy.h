#ifndef ENEMY_H
#define ENEMY_H
#include <SFML/Graphics.hpp>
#include <string>
class enemy
{
public:

    enemy(sf::String getname, int getlvl, int gethp , int getstr, int getdef,int getspeed, int getx, int gety,   std::string til);
    void draw(sf::RenderWindow & window);
    sf::FloatRect getBoundingbox();
    sf::Vector2f getPosition();
    void getdmg( int dmg);
    std::string name;
    int dmg();
    ~enemy();
    bool getDead();
    int getExp();
    int getLvl();
    void Move(sf:: Vector2f v);
    sf::Vector2f Move(sf::Time time, int j);
    bool getMov();
    void setMov(bool x);
protected:
private:
    int hp;
    bool Mov;
    sf::Vector2f LastPosition;
    bool isDead;
    int lvl;
    int str;
    int def;
    int speed;
    float maxhp;
    float positionX;
    float positionY;
    std::string tile;
    sf::Sprite przeciwnik;
    sf::FloatRect attackfild;
    void drawstats(sf::RenderWindow & window);

};

#endif // ENEMY_H
