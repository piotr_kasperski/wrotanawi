#ifndef LOADFILE_H
#define LOADFILE_H
#include <SFML/Graphics.hpp>
#include <vector>
#include <string>
#include "enemy.h"
class loadFile
{
public:
    std::vector<std::string> LoadStory(std::string);
    std::vector < enemy* > LoadEnemy(std::string);
    std::vector <std::string> LoadMainFile();
    loadFile();
protected:
private:
};

#endif // LOADFILE_H
