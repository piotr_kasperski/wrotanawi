#ifndef INTERFACE_H
#define INTERFACE_H
#include <SFML/Graphics.hpp>
#include "Gracz.h"
class Interface
{
public:
    Interface();
    virtual ~Interface();
    void draw(sf::RenderWindow &window ,Gracz &gracz);
    void update(Gracz & gracz );
    void setStat();
protected:
private:
    void stat(sf::RenderWindow &window, Gracz &gracz);
    std::vector <std::string> StatsToDraw;
    sf::RectangleShape interface;
    sf::RectangleShape life;
    sf::RectangleShape exp;
    sf::Font font;
    sf::Sprite staty;
    sf::Texture stattex;
    bool drawStat;
    sf::Text pozycja[8];

};

#endif // INTERFACE_H
