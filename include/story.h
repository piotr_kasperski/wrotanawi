#ifndef STORY_H
#define STORY_H
#include <vector>
#include "Plansza.h"
#include "enemy.h"
#include "Gracz.h"
#include <string>
class story
{
public:
    story();
    void nextMap(Plansza & level);
    void prevMap(Plansza & level);
    std::vector <enemy *> getLevelEnemy( int i);
    int getLevel();
    std::string getMap( int i);
    void update();
    void draw(sf::RenderWindow & window);

protected:
private:
    bool TellStory;
    std::vector <std::vector < enemy * > > vEnemy;

    int Level;

    std::vector <std::string> vMap;


};

#endif // STORY_H
