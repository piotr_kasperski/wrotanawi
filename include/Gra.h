#ifndef GRA_H
#define GRA_H
#include <SFML/Graphics.hpp>

#include <string>
#include "Engine.h"
#include "Gracz.h"
#include "Plansza.h"
#include "enemy.h"
class Gra
{
public:
    Gra(sf::RenderWindow & win);

    void Menu(sf::RenderWindow & window);
protected:
    enum StanGry {MENU , GAME , GAME_OVER, END};
    StanGry stan;
private:
};

#endif // GRA_H
