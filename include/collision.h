#ifndef COLLISION_H
#define COLLISION_H
#include "Gracz.h"
#include "Plansza.h"
#include "enemy.h"
#include <vector>
class collision
{
public:
    collision();
    void checkcollision(Gracz & gracz, Plansza &lvl, std::vector < enemy * >   V, sf::Vector2f ruch);
    void enemycol(enemy & Enemy, Plansza &lvl, std::vector < enemy * >   V, sf::Vector2f ruch);
protected:
private:
};

#endif // COLLISION_H
