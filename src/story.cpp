#include "story.h"
#include "Plansza.h"
#include "enemy.h"
#include "loadFile.h"
story::story()
{
    //pierwszy poziom
    story::Level=0;
    story::TellStory= true;
    loadFile *l=new loadFile;
    story::vMap=l->LoadMainFile();


    for (size_t i=0 ; i < story::vMap.size() ; i++)
    {

        story::vEnemy.push_back(l->LoadEnemy(story::vMap[i])) ;

    }
    delete l;
}


void story::nextMap(Plansza & level)
{
    story::Level+=1;
    level.WczytajPlansze(story::vMap[story::getLevel()]);
    story::TellStory= true;


}
void story::prevMap(Plansza & level)
{
    story::Level-=1;
    level.WczytajPlansze(story::vMap[story::getLevel()]);
}


std::vector <enemy *> story::getLevelEnemy(int i)
{
    return story::vEnemy[i];
}
int story::getLevel()
{
    return story::Level;
}
std::string story::getMap(int i)
{
    return story::vMap[i];
}

void story::update()
{


    for(size_t i = 0 ; i <story::vEnemy.size() ; i++ )
    {
        for(size_t j = 0 ; j<story::vEnemy[i].size() ; j++)
        {
            if (story::vEnemy[i][j]->getDead()) story::vEnemy[i].erase(story::vEnemy[i].begin()+j);
        }

    }

}

void story::draw(sf::RenderWindow & window)
{
    loadFile *file =  new loadFile;
    std::vector <std::string> str = file->LoadStory(story::getMap(story::getLevel()));
    delete file;
    sf::Font czcionka;
    czcionka.loadFromFile("data/font.ttf");
    std::vector <sf::Text> vtext;

    for (size_t i=0 ; i < str.size() ; i++)
    {
        sf::Text tekst;
        tekst.setFont(czcionka);
        tekst.setString(str[i]);
        tekst.setCharacterSize(14);
        tekst.setPosition(1024/2-tekst.getGlobalBounds().width/2 , 100+i*16 );

        vtext.push_back(tekst);
    }
    while (story::TellStory)
    {
        window.clear();
        for (size_t i=0 ; i<vtext.size() ; i++)
        {

            window.draw(vtext[i]);
        }
        window.display();

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        {

            story::TellStory = false;

        }
    }


}
//void story::gameOver(Gracz & gracz , sf::RenderWindow & window){

//}
