#include "Interface.h"
#include <cmath>
#include <sstream>
#include "Gracz.h"
Interface::Interface()
{
    Interface::interface.setSize(sf::Vector2f(128 , 19));
    Interface::interface.setPosition(7,7);
    Interface::interface.setFillColor(sf::Color::Blue);
    Interface::font.loadFromFile("data/font.ttf");
    Interface::stattex.loadFromFile("data/textury/stat.png");
    Interface::staty.setTexture(stattex);
    Interface::staty.setPosition(1024/2-staty.getGlobalBounds().width/2 , (768/2)-staty.getGlobalBounds().height/2);
    //ctor
}

Interface::~Interface()
{
    //dtor
}
void Interface::update(Gracz & gracz)
{

    Interface::life.setPosition(9,9);
    Interface::life.setSize(sf::Vector2f(floor(124*gracz.getLife())/gracz.getMafLife() ,  8  ));
    Interface::life.setFillColor(sf::Color::Red);

    Interface::exp.setPosition(9, 19);
    Interface::exp.setSize(sf::Vector2f(floor(124*gracz.getExp())/gracz.getNextLvl() , 4));
    Interface::exp.setFillColor(sf::Color::Cyan);
    Interface::StatsToDraw=gracz.getStats();
    Interface::StatsToDraw.push_back("");
    Interface::StatsToDraw.push_back("POWROT");

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::C))
    {
        Interface::setStat();
    }
    if(gracz.getStatPoint()>0)
    {
        std::ostringstream ss;
        std::string tmp="Punkty: ";
        ss << gracz.getStatPoint();
        tmp+=ss.str();
        Interface::StatsToDraw[6]+=tmp;

        for(size_t i=2 ; i<6 ; i++)
        {
            Interface::StatsToDraw[i]+= " +";
        }

    }
    for (size_t i = 0 ; i < Interface::StatsToDraw.size() ; i++)
    {
        Interface::pozycja[i].setFont(Interface::font);
        Interface::pozycja[i].setString(Interface::StatsToDraw[i]);
        Interface::pozycja[i].setCharacterSize(20);
        Interface::pozycja[i].setPosition((1024/2)-Interface::pozycja[i].getGlobalBounds().width/2 , Interface::staty.getPosition().y+(Interface::staty.getGlobalBounds().height/2-(Interface::StatsToDraw.size()*25)/2)+(i*25) );


    }


}
void Interface::draw(sf::RenderWindow & window, Gracz& gracz)
{
    window.draw(Interface::interface);
    window.draw(Interface::life);
    window.draw(Interface::exp);
    Interface::stat(window,gracz);

}
void Interface::stat(sf::RenderWindow &window, Gracz &gracz )
{
    sf::Vector2f mouse(sf::Mouse::getPosition(window));
    sf::Event zdarzenie;

    while (window.pollEvent(zdarzenie)&&window.isOpen())
    {
        if( zdarzenie.type == sf::Event::Closed ) window.close();

        if(gracz.getStatPoint()>0)
        {
            Interface::StatsToDraw[6]+="rozdaj punkty";
            if (Interface::pozycja[2].getGlobalBounds().contains(mouse) && zdarzenie.type == sf::Event::MouseButtonReleased &&    zdarzenie.mouseButton.button == sf::Mouse::Left )
            {
                gracz.incraseStat(1);
            }
            else if(Interface::pozycja[3].getGlobalBounds().contains(mouse) && zdarzenie.type == sf::Event::MouseButtonReleased &&    zdarzenie.mouseButton.button == sf::Mouse::Left )
            {
                gracz.incraseStat(2);
            }
            else if(Interface::pozycja[4].getGlobalBounds().contains(mouse) && zdarzenie.type == sf::Event::MouseButtonReleased &&    zdarzenie.mouseButton.button == sf::Mouse::Left )
            {
                gracz.incraseStat(3);
            }
            else if(Interface::pozycja[5].getGlobalBounds().contains(mouse) && zdarzenie.type == sf::Event::MouseButtonReleased &&    zdarzenie.mouseButton.button == sf::Mouse::Left )
            {
                gracz.incraseStat(4);
            }
            for(int i=2; i<6; i++)
                if (Interface::pozycja[i].getGlobalBounds().contains(mouse))
                    Interface::pozycja[i].setColor(sf::Color::Cyan);
                else Interface::pozycja[i].setColor(sf::Color::White);
        }
        else for(size_t i=2; i<6; i++) Interface::pozycja[i].setColor(sf::Color::White);

        if(Interface::pozycja[7].getGlobalBounds().contains(mouse) && zdarzenie.type == sf::Event::MouseButtonReleased &&    zdarzenie.mouseButton.button == sf::Mouse::Left )
        {
            Interface::drawStat=false;
        }

        if (Interface::pozycja[7].getGlobalBounds().contains(mouse))
            Interface::pozycja[7].setColor(sf::Color::Cyan);
        else Interface::pozycja[7].setColor(sf::Color::White);
    }

    if(Interface::drawStat)
    {

        window.draw(staty);
        for (size_t i = 0 ; i < Interface::StatsToDraw.size() ; i++)
        {
            window.draw(Interface::pozycja[i]);
        }
    }

}
void Interface::setStat()
{
    Interface::drawStat=true;
}

