#include "Gra.h"





Gra::Gra(sf::RenderWindow & win)
{

    Gra::Menu( win );

}

void Gra::Menu(sf::RenderWindow & window)
{
    Gra::stan=MENU;
    sf::Font czcionka;
    czcionka.loadFromFile("data/font.ttf");//wczytanie czciaonki


    sf::Text tekst;
    //ustawienie tekstu
    tekst.setFont(czcionka);
    tekst.setString("Wrota Nawii");
    tekst.setCharacterSize(80);
    tekst.setPosition(1024/2-tekst.getGlobalBounds().width/2 , 80);


    const int IloscPozycji = 2;

    sf::Text pozycja[IloscPozycji];
    std::string nazwapozycji[] = {"New Game" , "Exit"};
    for (int i= 0 ; i<IloscPozycji ; i++)
    {
        pozycja[i].setFont(czcionka);
        pozycja[i].setString(nazwapozycji[i]);
        pozycja[i].setCharacterSize(60);
        pozycja[i].setPosition(1024/2-pozycja[i].getGlobalBounds().width/2 , 240+i*100 );



    }

    while (Gra::stan == MENU)
    {
        sf::Vector2f mouse(sf::Mouse::getPosition(window));
        sf::Event zdarzenie;

        while (window.pollEvent(zdarzenie))
        {
            if (pozycja[0].getGlobalBounds().contains(mouse) && zdarzenie.type == sf::Event::MouseButtonReleased &&    zdarzenie.mouseButton.button == sf::Mouse::Left )
            {

                Engine nowy(window);

                Gra::stan = GAME;
            }
            else if (pozycja[1].getGlobalBounds().contains(mouse) && zdarzenie.type == sf::Event::MouseButtonReleased &&    zdarzenie.mouseButton.button == sf::Mouse::Left )
            {
                Gra::stan = END;
                //Koniec gry
            }/*else if(pozycja[numer pozycji menu].getGlobalBounds().contains(mouse) && zdarzenie.type == sf::Event::MouseButtonReleased &&    zdarzenie.mouseButton.button == sf::Mouse::Left ) */
        }
        for(int i=0; i<IloscPozycji; i++)
            if (pozycja[i].getGlobalBounds().contains(mouse))
                pozycja[i].setColor(sf::Color::Cyan);
            else pozycja[i].setColor(sf::Color::White);



        window.clear();

        window.draw( tekst );
        for(int i=0; i<IloscPozycji; i++)
            window.draw(pozycja[i]);

        window.display();
    }

}




