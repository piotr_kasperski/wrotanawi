#include "Gracz.h"
#include <SFML/Graphics.hpp>
#include "cmath"
#include <sstream>
bool Gracz::up , Gracz::down , Gracz::left , Gracz::right;
Gracz::Gracz()
{
    tekstura.loadFromFile( "data/textury/character.png" );
    Gracz::postac.setTexture( tekstura );
    Gracz::postac.setTextureRect(sf::IntRect(32,64,32,32));
    Gracz::postac.setOrigin(16,16);
    Gracz::postac.setPosition(16+32,16+32);
    Gracz::atackfild.setOrigin(16,16);
    Gracz::statystyki.Level=1;
    Gracz::statystyki.MaxLife=100;
    Gracz::statystyki.Life=100;
    Gracz::statystyki.Strengh=10;
    Gracz::statystyki.Speed=10;
    Gracz::statystyki.Defens=10;
    Gracz::statystyki.Experience=0;
    Gracz::setPosition(16*32 , 12*32);
    Gracz::isAlive= true;
}

void Gracz::Draw(sf::RenderWindow & win)
{
    if(Gracz::isAlive)  win.draw(Gracz::postac);
    else Gracz::GameOver(win);
}
void Gracz::Move(sf::Vector2f v)
{
    Gracz::postac.move(v);
}
sf::Vector2f Gracz::getPosition()
{
    return Gracz::postac.getPosition();
}
void Gracz::setPosition(int x , int y)
{
    Gracz::postac.setPosition( x , y);
}
sf::FloatRect Gracz::getBoundingbox()
{
    return Gracz::postac.getGlobalBounds();
}
void Gracz::update()
{
    Gracz::statystyki.NextLvl=Gracz::statystyki.Level*100;
    if(Gracz::statystyki.Life<=0) Gracz::isAlive=false;
    Gracz::LevelUp();
    int rotation;
    Gracz::up = false;
    Gracz::down = false;
    Gracz::left = false;
    Gracz::right = false;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        Gracz::up = true;
        rotation=0;
        Gracz::postac.setTextureRect(sf::IntRect(32,0,32,32));
        Gracz::atackfild.setPosition(Gracz::postac.getPosition().x , Gracz::postac.getPosition().y-32);
        //Gracz::postac.setRotation(0);

    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        Gracz::down = true;
        rotation=180;
        rotation=0;
        Gracz::postac.setTextureRect(sf::IntRect(32,64,32,32));
        Gracz::atackfild.setPosition(Gracz::postac.getPosition().x , Gracz::postac.getPosition().y+32);
        // Gracz::postac.setRotation(180);
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        Gracz::left = true;
        rotation=270;
        rotation=0;
        Gracz::postac.setTextureRect(sf::IntRect(0,96,32,32));
        Gracz::atackfild.setPosition(Gracz::postac.getPosition().x-32 , Gracz::postac.getPosition().y);
        // Gracz::postac.setRotation(270);

    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        Gracz::right = true;
        rotation=90;
        // Gracz::postac.setRotation(90);
        //rotation=0;
        Gracz::postac.setTextureRect(sf::IntRect(64,32,32,32));
        Gracz::atackfild.setPosition(Gracz::postac.getPosition().x+32 , Gracz::postac.getPosition().y);

    }
    if(rotation==0)

        //  Gracz::postac.setTextureRect(sf::IntRect(32,0,32,32));
        if(rotation==180)
            //
            // Gracz::postac.setTextureRect(sf::IntRect(32,64,32,32));
            if(rotation==270)
                //   Gracz::atackfild.setPosition(Gracz::postac.getPosition().x-32 , Gracz::postac.getPosition().y);
                //  Gracz::postac.setTextureRect(sf::IntRect(32,96,32,32));
                if(rotation==90)
                    //   Gracz::atackfild.setPosition(Gracz::postac.getPosition().x+32 , Gracz::postac.getPosition().y);
                    //  Gracz::postac.setTextureRect(sf::IntRect(32,32,32,32));
                    Gracz::LifeRegeneration();
}
void Gracz::LifeRegeneration()
{
    if(Gracz::statystyki.Life<Gracz::statystyki.MaxLife)
    {
        Gracz::statystyki.Life+=Gracz::statystyki.Speed*0.01;

    }
    else Gracz::statystyki.Life=Gracz::statystyki.MaxLife;

}

sf::Vector2f Gracz::Move(sf::Time time)
{
    sf::Vector2f v(0.0,0.0);
    if(Gracz::up)
        v.y -= Gracz::statystyki.Speed*1.6;
    if(Gracz::down)
        v.y += Gracz::statystyki.Speed*1.6;

    if(Gracz::left)
        v.x -=Gracz::statystyki.Speed*1.6;

    if(Gracz::right)
        v.x += Gracz::statystyki.Speed*1.6;

    return v*time.asSeconds();



}
sf::Vector2f Gracz::getAtack()
{
    return Gracz::atackfild.getPosition();
}
void Gracz::getdmg(float dmg)
{
    Gracz::statystyki.Life-=(dmg/Gracz::statystyki.Defens);

}
int Gracz::dmg()
{
    return floor(Gracz::statystyki.Strengh*((Gracz::statystyki.Speed*3.2)/10));
}
float Gracz::getLife()
{
    return Gracz::statystyki.Life;
}
int Gracz::getExp()
{
    return Gracz::statystyki.Experience;
}
int Gracz::getMafLife()
{
    return Gracz::statystyki.MaxLife;
}
int Gracz::getNextLvl()
{
    return Gracz::statystyki.NextLvl;
}
void Gracz::experience(int exp)
{
    Gracz::statystyki.Experience+=exp;
}
int Gracz::getLvl()
{
    return Gracz::statystyki.Level;
}
void Gracz::GameOver(sf::RenderWindow &window)
{
    sf::Font czcionka;
    czcionka.loadFromFile("data/font.ttf");
    sf::Text tekst;
    tekst.setFont(czcionka);
    tekst.setString("GAME OVER");
    tekst.setCharacterSize(140);
    tekst.setColor(sf::Color::Black);
    tekst.setPosition(1024/2-tekst.getGlobalBounds().width/2 , (768/2)-tekst.getGlobalBounds().height/2 );
    window.clear(sf::Color::Red);
    window.draw(tekst);
    window.display();
}
void Gracz::LevelUp()
{
    if(Gracz::statystyki.Experience>=Gracz::statystyki.NextLvl)
    {
        Gracz::statystyki.Experience-=Gracz::statystyki.NextLvl;
        Gracz::statystyki.Level++;
        Gracz::statystyki.StatPoint+=5;

    }
}
std::vector <std::string> Gracz::getStats()
{
    std::vector <std::string> v;
    std::ostringstream ss;

    std::string statystic;
    statystic="Level: ";
    ss << Gracz::statystyki.Level;
    statystic+=ss.str();
    v.push_back(statystic);

    statystic="Exp: ";
    ss.str(std::string());
    ss<< Gracz::statystyki.Experience;
    statystic+=ss.str();
    statystic+="/";
    ss.str(std::string());
    ss << Gracz::statystyki.NextLvl;
    statystic+=ss.str();
    v.push_back(statystic);

    statystic="Life: ";
    ss.str(std::string());
    ss << floor(Gracz::statystyki.Life);
    statystic+=ss.str();
    statystic+="/";
    ss.str(std::string());
    ss << Gracz::statystyki.MaxLife;
    statystic+=ss.str();
    v.push_back(statystic);

    statystic="Strengt: ";
    ss.str(std::string());
    ss << Gracz::statystyki.Strengh;
    statystic+=ss.str();
    v.push_back(statystic);

    statystic="Defense: ";
    ss.str(std::string());
    ss << Gracz::statystyki.Defens;
    statystic+=ss.str();
    v.push_back(statystic);

    statystic="Speed";
    ss.str(std::string());
    ss << Gracz::statystyki.Speed;
    statystic+=ss.str();
    v.push_back(statystic);


    return v;
}
void Gracz::incraseStat(int i)
{
    switch(i)
    {
    case 1:
        Gracz::statystyki.MaxLife+=50;
        Gracz::statystyki.Life+=50;
        break;

    case 2:
        Gracz::statystyki.Strengh+=10;
        break;

    case 3:
        Gracz::statystyki.Defens+=10;
        break;

    case 4:
        Gracz::statystyki.Speed+=10;
        break;
    }
    Gracz::statystyki.StatPoint--;
}
int Gracz::getStatPoint()
{
    return Gracz::statystyki.StatPoint;
}

